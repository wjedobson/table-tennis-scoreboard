import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { GameService } from './services/game/game.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'table-tennis-scoreboard';
  winner!: string | null;
  winner$: Subscription = new Subscription;

  constructor(private gameService: GameService) {}

  ngOnInit(): void {
    this.gameService.resetGame();
    this.winner$ = this.gameService.winner$.subscribe(winner => {
      this.winner = winner;
    })
  }

  createNewGame(): void {
    this.gameService.createNewGame();
  }


}
