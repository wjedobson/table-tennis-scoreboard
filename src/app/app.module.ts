import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button'
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { AppComponent } from './app.component';
import { NewGameModalComponent } from './components/new-game-modal/new-game-modal.component';
import { PlayerViewComponent } from './components/player-view/player-view.component';
import { WinnerViewComponent } from './components/winner-view/winner-view.component';
import { BoardViewComponent } from './components/board-view/board-view.component';

@NgModule({
  declarations: [
    AppComponent,
    NewGameModalComponent,
    PlayerViewComponent,
    WinnerViewComponent,
    BoardViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
