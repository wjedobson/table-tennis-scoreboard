import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Player } from 'src/app/interfaces/player.interface';
import { GameService } from 'src/app/services/game/game.service';

@Component({
  selector: 'app-board-view',
  templateUrl: './board-view.component.html',
  styleUrls: ['./board-view.component.scss']
})
export class BoardViewComponent implements OnInit {

  p1!: Player;
  p2!: Player;
  p1ToServe!: boolean;

  p1$: Subscription = new Subscription;
  p2$: Subscription = new Subscription;
  p1ToServe$: Subscription = new Subscription;

  constructor(private gameService: GameService) { }

  ngOnInit(): void {
    this.p1$ = this.gameService.player1$.subscribe(player => this.p1 = player);
    this.p2$ = this.gameService.player2$.subscribe(player => this.p2 = player);
    this.p1ToServe$ = this.gameService.p1ToServe$.subscribe(toServe => this.p1ToServe = toServe);
  }

  updateScore(player: Player): void {
    this.gameService.updateScore(player.name === this.p1.name)
  }

  hasMatchPoint(player: Player, opp: Player): boolean {
    return this.gameService.returnMatchPoint(player.score, opp.score);
  }

}
