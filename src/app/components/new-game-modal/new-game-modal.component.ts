import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-game-modal',
  templateUrl: './new-game-modal.component.html',
  styleUrls: ['./new-game-modal.component.scss']
})
export class NewGameModalComponent implements OnInit {
  player1: string | null = null;
  player2: string | null = null;
  targetScore: number | null = null;

  constructor() { }

  ngOnInit(): void {
  }

}
