import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Player } from 'src/app/interfaces/player.interface';

@Component({
  selector: 'app-player-view',
  templateUrl: './player-view.component.html',
  styleUrls: ['./player-view.component.scss']
})
export class PlayerViewComponent implements OnInit {
  @Input() player!: Player;
  @Input() opp!: Player;
  @Input() toServe!: boolean;
  @Input() hasMatchPoint!: boolean;
  @Output() updateScore = new EventEmitter<Player>();

  constructor() { }

  ngOnInit(): void {}

  onScoreClicked(): void {
    this.updateScore.emit();
  }

}
