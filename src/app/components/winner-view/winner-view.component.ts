import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { GameService } from 'src/app/services/game/game.service';

@Component({
  selector: 'app-winner-view',
  templateUrl: './winner-view.component.html',
  styleUrls: ['./winner-view.component.scss']
})
export class WinnerViewComponent implements OnInit {
  @Input() winner: string = '';

  constructor(public gameService: GameService) { }

  ngOnInit(): void {}

}
