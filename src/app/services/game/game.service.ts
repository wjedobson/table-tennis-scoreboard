import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { NewGameModalComponent } from 'src/app/components/new-game-modal/new-game-modal.component';
import { Player } from 'src/app/interfaces/player.interface';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  defaultPlayer: Player = {
    name: 'player',
    score: 0
  }

  defaultPlayer2: Player = {
    name: 'player 2',
    score: 0
  }
  private player1 = new BehaviorSubject<Player>({...this.defaultPlayer});
  private player2 = new BehaviorSubject<Player>({...this.defaultPlayer2});
  private winner = new BehaviorSubject<string | null>(null);
  private targetScore = new BehaviorSubject<number>(5);
  private p1ToServe = new BehaviorSubject<boolean>(true);
  public player1$ = this.player1.asObservable();
  public player2$ = this.player2.asObservable();
  public winner$ = this.winner.asObservable();
  public p1ToServe$ = this.p1ToServe.asObservable();

  constructor(private dialog: MatDialog) { }

  updateScore(isPlayer1: boolean): void {
    let player: Player;
    if (isPlayer1) {
      player = this.player1.value;
    } else {
      player = this.player2.value;
    }
    player.score++;
    if (isPlayer1) {
      this.player1.next(player);
    } else {
      this.player2.next(player);
    }
    this.calculateWinner();
    if (!this.winner.getValue()) {
      this.updateToServe();
    }
  }

  calculateWinner() {
    const p1 = this.player1.getValue()
    const p2 = this.player2.getValue();
    if ((p1.score >= this.targetScore.value) && (p1.score >= p2.score + 2)) {
      this.winner.next(p1.name);
    } else if ((p2.score >= this.targetScore.value) && (p2.score >= p1.score + 2)) {
      this.winner.next(p2.name);
    }
  }

  updateToServe(): void {
    const p1Score = this.player1.getValue().score;
    const p2Score = this.player2.getValue().score;
    const p1ToServe = this.p1ToServe.getValue();
    if ((p1Score + p2Score) % 5 === 0) {
      this.p1ToServe.next(!p1ToServe);
    }
  }

  resetGame(): void {
    this.winner.next(null);
    const player1 = this.player1.getValue();
    const player2 = this.player2.getValue();
    player1.score = 0;
    player2.score = 0;
    this.player1.next({...player1});
    this.player2.next({...player2});
    this.randomServe();
  }

  returnMatchPoint(player: number, opp: number): boolean {
    return ((player >= this.targetScore.getValue() - 1) && (player >= opp + 1));
  }

  createNewGame(): void {
    const newPlayerRef = this.dialog.open(NewGameModalComponent);
    newPlayerRef.afterClosed().subscribe(result => {
      const player1: Player = {
        name: result.player1,
        score: 0
      };
      const player2: Player = {
        name: result.player2,
        score: 0
      }
      this.player1.next(player1);
      this.player2.next(player2);
      this.targetScore.next(result.targetScore);
      if (this.winner.getValue()) {
        this.winner.next(null);
      }
      this.randomServe();
    })
  }

  randomServe(): void {
    this.p1ToServe.next(Math.floor(Math.random() * 100) > 50);
  }
}
